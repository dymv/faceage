//
//  Created by eugene on 28.04.12.
//
#import "FaceEngine.h"
#import "Defines.h"
#import "FAFaceAge.h"


@implementation FaceEngine {

}


- (MKNetworkOperation *)ageForURL:(NSString *)url onCompletion:(FaceResponseBlock)completion onError:(ErrorBlock)errorBlock {
    MKNetworkOperation *operation = [self operationWithPath:[NSString stringWithFormat:@"faces/recognize.json?api_key=%@&api_secret=%@&uids=eugene@dymov&attributes=all&urls=%@", API_KEY, API_SECRET, url] params:nil httpMethod:@"POST"];

    [operation onDownloadProgressChanged:^(double progress) {
        DLog(@"dow: %.2f", progress * 100.0);
    }];

    [operation
            onCompletion:^(MKNetworkOperation *completedOperation){
                FAFaceAge *faceAge = [[FAFaceAge alloc] init];
                [faceAge setMax:-1];
                [faceAge setEst:-1];
                [faceAge setMin:-1];
                NSDictionary *responseJSON = [operation responseJSON];
                if (responseJSON.count) {
                    NSArray *photos = [responseJSON objectForKey:@"photos"];
                    if (photos.count) {
                        NSArray *tags = [[photos objectAtIndex:0] objectForKey:@"tags"];
                        if (tags.count) {
                            NSDictionary *attributes = [[tags objectAtIndex:0] objectForKey:@"attributes"];
                            NSDictionary *ageEstDictionary = [attributes objectForKey:@"age_est"];
                            NSDictionary *ageMaxDictionary = [attributes objectForKey:@"age_max"];
                            NSDictionary *ageMinDictionary = [attributes objectForKey:@"age_min"];

                            NSInteger ageEst = [[ageEstDictionary objectForKey:@"value"] integerValue];
                            NSInteger ageMax = [[ageMaxDictionary objectForKey:@"value"] integerValue];
                            NSInteger ageMin = [[ageMinDictionary objectForKey:@"value"] integerValue];


                            [faceAge setMax:ageMax];
                            [faceAge setEst:ageEst];
                            [faceAge setMin:ageMin];
                        } else {
                            DLog(@"tags array is empty");
                        }
                    } else {
                        DLog(@"photos array is empty");
                    }
                } else {
                    DLog(@"responseJSON is empty");
                }

                completion(faceAge);
            }
                 onError:^(NSError *error) {
                     errorBlock(error);
                 }
    ];

    [self enqueueOperation:operation];

    return operation;
}


@end