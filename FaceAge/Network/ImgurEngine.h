//
//  Created by eugene on 28.04.12.
//
#import <Foundation/Foundation.h>
#import "Defines.h"

@interface ImgurEngine : MKNetworkEngine

- (MKNetworkOperation *)urlForImage:(UIImage *)image onCompletion:(ImgurResponseBlock)completion onProgress:(ProgressBlock)progressBlock onError:(ErrorBlock)error;

@end