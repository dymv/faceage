//
//  Created by eugene on 28.04.12.
//
#import <Foundation/Foundation.h>
#import "Defines.h"

@interface FaceEngine : MKNetworkEngine

-(MKNetworkOperation *)ageForURL:(NSString *)url
                          onCompletion:(FaceResponseBlock)completion
                               onError:(ErrorBlock)error;

@end