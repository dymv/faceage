//
//  Created by eugene on 28.04.12.
//
#import "ImgurEngine.h"
#import "Defines.h"
#import "UIImage+Resize.h"


@implementation ImgurEngine {

}
- (MKNetworkOperation *)urlForImage:(UIImage *)image onCompletion:(ImgurResponseBlock)completion onProgress:(ProgressBlock)progressBlock onError:(ErrorBlock)errorBlock {
    MKNetworkOperation *operation = [self operationWithPath:[NSString stringWithFormat:@"2/upload.json?key=%@", IMGUR_API_KEY] params:nil httpMethod:@"POST"];

    CGSize currentImageSize = image.size;

    DLog(@"currentSize: %@", NSStringFromCGSize(currentImageSize));

    UIImage *smallerImage = [image resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:CGSizeMake(640, 480) interpolationQuality:kCGInterpolationMedium];

    DLog(@"newSize: %@", NSStringFromCGSize(smallerImage.size));

    NSData *imageData = UIImagePNGRepresentation(smallerImage);
    NSAssert(!!imageData, @"Image data couldnt be nil");
    [operation addData:imageData forKey:@"image"];

    [operation onUploadProgressChanged:^(double progress) {
        DLog(@"upl: %.2f", progress * 100.0);
        progressBlock(progress);
    }];

    [operation onDownloadProgressChanged:^(double progress) {
        DLog(@"dow: %.2f", progress * 100.0);
    }];

    [operation
            onCompletion:^(MKNetworkOperation *completedOperation){
                NSDictionary *responseJSON = [operation responseJSON];
                NSDictionary *upload = [responseJSON objectForKey:@"upload"];
                NSDictionary *links = [upload objectForKey:@"links"];
                NSString *original = [links objectForKey:@"original"];
                completion(original);
            }
                 onError:^(NSError *error) {
                     errorBlock(error);
                 }
    ];

    [self enqueueOperation:operation];

    return operation;
}

@end