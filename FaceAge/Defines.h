@class FAFaceAge;
#define API_KEY @"3082dffca30b532d41c8c0a37ecd521a"
#define API_SECRET @"08f57b319c016ddc7772451c99bec3ee"

#define IMGUR_API_KEY @"6089a61e36c5bed4a3b290142b30318f"

typedef void (^ProgressBlock)(double progress);
typedef void (^ImgurResponseBlock)(NSString *imageURL);
typedef void (^FaceResponseBlock)(FAFaceAge *faceAge);
typedef void (^ErrorBlock)(NSError* error);

#define EXLT_FONT   @"AvantGardeGothicExLtITC-Reg"
#define BK_FONT     @"AvantGardeGothicBkITC-Reg"