//
//  FAAppDelegate.h
//  FaceAge
//
//  Created by Eugene Dymov on 27.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FAViewController;

@interface FAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) FAViewController *viewController;

@end
