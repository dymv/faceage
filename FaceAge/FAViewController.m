//
//  FAViewController.m
//  FaceAge
//
//  Created by Eugene Dymov on 27.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <CoreGraphics/CoreGraphics.h>
#import "FAViewController.h"
#import "FaceEngine.h"
#import "ImgurEngine.h"
#import "FAFaceAge.h"

@interface FAViewController (Private) <UIImagePickerControllerDelegate>
- (void)_clickedWhatsMyAgeButton:(id)sender;


@end

@implementation FAViewController (Private)


- (void)_clickedWhatsMyAgeButton:(id)sender {
    [self.estLabel setText:@""];
    [self.minLabel setText:@""];
    [self.maxLabel setText:@""];

#if TARGET_IPHONE_SIMULATOR
    [self setImage:[UIImage imageNamed:@"camera.png"]];
#else
    UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
    [pickerController setDelegate:self];
    [pickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
    [self presentModalViewController:pickerController animated:YES];
#endif
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo {
    [self dismissModalViewControllerAnimated:YES];
    [self setImage:image];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissModalViewControllerAnimated:YES];
    [self setImage:nil];
}


@end

@implementation FAViewController {
    UIImageView *_imageView, *_loadingImageView;
    UIImage *_image;
    FaceEngine *_faceEngine;
    ImgurEngine *_imgurEngine;
    UIButton *_whatsMyAgeButton;
    UILabel *_estLabel, *_minLabel, *_maxLabel;
    UIProgressView *_progress;
}
@synthesize imageView = _imageView;
@synthesize image = _image;
@synthesize faceEngine = _faceEngine;
@synthesize imgurEngine = _imgurEngine;
@synthesize loadingImageView = _loadingImageView;
@synthesize whatsMyAgeButton = _whatsMyAgeButton;
@synthesize estLabel = _estLabel;
@synthesize minLabel = _minLabel;
@synthesize maxLabel = _maxLabel;
@synthesize progress = _progress;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
//        [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background_Noisy.png"]]];
    }

    return self;
}

- (void)getFaceInfoFromURL:(NSString *)url {
    self.faceEngine = [[FaceEngine alloc] initWithHostName:@"api.face.com"];
    [self.faceEngine ageForURL:url
                  onCompletion:^(FAFaceAge *faceAge) {
                      [self.progress setProgress:1.0];
                      [self.whatsMyAgeButton setHidden:NO];
                      [self.loadingImageView setHidden:YES];
                      [self.progress setHidden:YES];
                      NSString *estText = nil;
                      if (faceAge.est <= 0) {
                          estText = @"no face";
                      } else {
                          estText = [NSString stringWithFormat:@"%d", faceAge.est];
                      }

                      CGSize size = [estText sizeWithFont:self.estLabel.font];
                      [self.estLabel setFrame:CGRectMake(floorf((self.view.frame.size.width - size.width) / 2.0), floorf((self.view.frame.size.height - size.height) / 3.0), size.width, size.height)];
                      [self.estLabel setText:estText];
                  }
                       onError:^(NSError *error) {
                           DLog(@"Doh! %@", error.localizedDescription);
                           [self.whatsMyAgeButton setHidden:NO];
                           [self.loadingImageView setHidden:YES];
                           [self.progress setHidden:YES];
                       }];
}

- (void)setImage:(UIImage *)anImage {
    _image = anImage;

//    [self.imageView setImage:self.image];
    [self.whatsMyAgeButton setHidden:YES];
    [self.loadingImageView setHidden:NO];
    [self.progress setProgress:0.0];
    [self.progress setHidden:NO];

    self.imgurEngine = [[ImgurEngine alloc] initWithHostName:@"api.imgur.com"];
    [self.imgurEngine     urlForImage:self.image onCompletion:^(NSString *imageURL){
        DLog(@"%@", imageURL);
        [self getFaceInfoFromURL:imageURL];
    } onProgress:^void(double progress) {
        [self.progress setProgress:(float) progress/1.1];
    }
                              onError:^(NSError *error) {
                                  DLog(@"%@", error.localizedDescription);
                                  [self.whatsMyAgeButton setHidden:NO];
                                  [self.loadingImageView setHidden:YES];
                                  [self.progress setHidden:YES];
                              }];
}

- (void)onlyButtonVisible:(BOOL)onlyButton {
    if (onlyButton) {

    } else {

    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    self.loadingImageView = [[UIImageView alloc] init];
    [self.loadingImageView setImage:[UIImage imageNamed:@"loading.png"]];
    [self.loadingImageView sizeToFit];
    CGSize loadingImageSize = self.loadingImageView.frame.size;
    [self.loadingImageView setFrame:CGRectMake(floorf((self.view.frame.size.width - loadingImageSize.width) / 2.0), floorf((self.view.frame.size.height - loadingImageSize.height) / 2.0), loadingImageSize.width, loadingImageSize.height)];
    [self.loadingImageView setHidden:YES];

    self.progress = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
    [self.progress setFrame:CGRectMake(self.loadingImageView.frame.origin.x, self.loadingImageView.frame.origin.y + self.loadingImageView.frame.size.height + 5, self.loadingImageView.frame.size.width, 10)];
    [self.progress setHidden:YES];


    _whatsMyAgeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_whatsMyAgeButton setImage:[UIImage imageNamed:@"AttachCamera.png"] forState:UIControlStateNormal];
    [_whatsMyAgeButton setImage:[UIImage imageNamed:@"AttachCamera_pressed.png"] forState:UIControlStateHighlighted];
    [_whatsMyAgeButton addTarget:self action:@selector(_clickedWhatsMyAgeButton:) forControlEvents:UIControlEventTouchUpInside];
    [_whatsMyAgeButton setFrame:CGRectMake(floorf((self.view.frame.size.width - kButtonWidth) / 2.0), floorf((self.view.frame.size.height - kButtonHeight) / 2.0), kButtonWidth, kButtonHeight)];

    _imageView = [[UIImageView alloc] init];
    [_imageView setImage:[UIImage imageNamed:@"Background_Noisy.png"]];
    [_imageView setAlpha:0.4];
    [_imageView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];

    self.estLabel = [[UILabel alloc] init];
    [self.estLabel setFont:[UIFont fontWithName:EXLT_FONT size:45]];
    CGSize size = [@"99" sizeWithFont:self.estLabel.font];
    [self.estLabel setFrame:CGRectMake(floorf((self.view.frame.size.width - size.width) / 2.0), floorf((self.view.frame.size.height - size.height) / 3.0), size.width, size.height)];
    [self.estLabel setTextColor:[UIColor whiteColor]];
    [self.estLabel setBackgroundColor:[UIColor clearColor]];

    self.minLabel = [[UILabel alloc] init];
    [self.minLabel setFont:[UIFont fontWithName:BK_FONT size:13]];
    size = [@"99" sizeWithFont:self.minLabel.font];
    [self.minLabel setFrame:CGRectMake(floorf((self.view.frame.size.width - size.width) / 2.0) - self.estLabel.frame.size.width / 1.2, self.estLabel.frame.origin.y + self.estLabel.frame.size.height - size.height * 2, size.width, size.height)];
    [self.minLabel setTextColor:[UIColor colorWithWhite:0.7 alpha:1.0]];
    [self.minLabel setBackgroundColor:[UIColor clearColor]];
    [self.minLabel setHidden:YES];

    self.maxLabel = [[UILabel alloc] init];
    [self.maxLabel setFont:[UIFont fontWithName:BK_FONT size:13]];
    size = [@"99" sizeWithFont:self.maxLabel.font];
    [self.maxLabel setFrame:CGRectMake(floorf((self.view.frame.size.width - size.width) / 2.0) + self.estLabel.frame.size.width / 1.2, self.minLabel.frame.origin.y, size.width, size.height)];
    [self.maxLabel setTextColor:[UIColor colorWithWhite:0.7 alpha:1.0]];
    [self.maxLabel setBackgroundColor:[UIColor clearColor]];
    [self.maxLabel setHidden:YES];

    [self.view addSubview:_imageView];
    [self.view addSubview:_whatsMyAgeButton];
    [self.view addSubview:self.loadingImageView];
    [self.view addSubview:self.progress];
    [self.view addSubview:self.estLabel];
    [self.view addSubview:self.minLabel];
    [self.view addSubview:self.maxLabel];

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
