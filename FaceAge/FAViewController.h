//
//  FAViewController.h
//  FaceAge
//
//  Created by Eugene Dymov on 27.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FaceEngine;
@class ImgurEngine;

static int const kButtonWidth = 88;
static int const kButtonHeight = 89;

@interface FAViewController : UIViewController <UINavigationControllerDelegate>

@property(nonatomic, strong) UIImageView *imageView;
@property(nonatomic, strong) UIImage *image;
@property(nonatomic, strong) FaceEngine *faceEngine;
@property(nonatomic, strong) ImgurEngine *imgurEngine;
@property(nonatomic, strong) UIImageView *loadingImageView;
@property(nonatomic, strong) UIButton *whatsMyAgeButton;
@property(nonatomic, strong) UILabel *estLabel;
@property(nonatomic, strong) UILabel *minLabel;
@property(nonatomic, strong) UILabel *maxLabel;
@property(nonatomic, strong) UIProgressView *progress;


@end
