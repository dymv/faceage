//
//  Created by eugene on 28.04.12.
//
#import <Foundation/Foundation.h>


@interface FAFaceAge : NSObject
@property(nonatomic, assign) NSInteger min;
@property(nonatomic, assign) NSInteger est;
@property(nonatomic, assign) NSInteger max;


@end