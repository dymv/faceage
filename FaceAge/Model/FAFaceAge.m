//
//  Created by eugene on 28.04.12.
//
#import "FAFaceAge.h"


@implementation FAFaceAge {
    NSInteger _min, _est, _max;
}
@synthesize min = _min;
@synthesize est = _est;
@synthesize max = _max;

- (NSString *)description {
    return [[super description] stringByAppendingFormat:@"{%d,%d,%d}", self.min, self.est, self.max];
}


@end